borgbackup_client
=========

Configure a client system to use borgbackup[^1].  Clients will connect to the
master thru SSH for performing the backup.

Tested on OpenBSD & Debian; small adjustments are needed on other systems;
specific distro naming of the package comes to mind.

Requirements
------------

Clients using this role need a working SSH public key authentication setup
towards the borgbackup server.

A borgbackup repository needs to have been configured already.

Role Variables
--------------

| Variable        | Default               | Description                                                                                  |
|-----------------|-----------------------|----------------------------------------------------------------------------------------------|
| `borgc_version` | `1.1.10`              | Version of borgbackup to install                                                             |
| `borgc_pass`    | `N/A`                 | Pre-configured passphrase for accessing the backup repository.  Stored in `/root/.borgpass`. |
| `borgc_log_dir` | `/var/log/borgbackup` | Directory to log output of backup jobs                                                       |
| `borgc_jobs`    | `[]`                  | List of backup jobs that should run on this client/group.  Details below.                    |

### borgc_jobs ###

List of borgbackup jobs to run on this client or group of clients.  For each
of the items listed, one script will be installed under
`/usr/local/bin/borg_<name>`; said job will be triggered per the cron entry
described in the item.  Details & example below:

| Variable  | Description                                                                               |
|-----------|-------------------------------------------------------------------------------------------|
| `name`    | Name of the backup job.                                                                   |
| `minute`  | Respects `ansible.builtin.cron` syntax.  Optional unless all other cron vars are omitted. |
| `hour`    | Respects `ansible.builtin.cron` syntax.  Optional unless all other cron vars are omitted. |
| `weekday` | Respects `ansible.builtin.cron` syntax.  Optional unless all other cron vars are omitted. |
| `month`   | Respects `ansible.builtin.cron` syntax.  Optional unless all other cron vars are omitted. |
| `notify`  | Shell command to call when the job is *successful*; think dead man's switch.              |
| `job`     | Actual borg command one needs to run *without* the borg binary.                           |

#### Example ####

```
borgc_jobs:
  - name: backup-the-repos
    minute: 00
    hour: 20
    weekday: 5
    notify: 'curl -m 10 --retry 5 https://some-dead-man-switch-type.service'
    job: |
      create --stats backup-user@backup-server.lan:/backups/repos/git-server.lan::repos-$(date +%Y-%m-%d) /home/git/repos/
```

This job will create the `/usr/local/bin/borg_backup-the-repos` script for
backing up `git-server.lan`'s `/home/git/repos` directory on `backup-server.lan`.
Should this not be clear, this is borgbackup
[create](https://borgbackup.readthedocs.io/en/stable/usage/create.html) syntax.
The role takes care of finding the proper `borg` binary and setting up logging
inside `borgc_log_dir`.

The script will be installed as a cron job per the listed schedule.

`notify` specifies a command that should run if the backup completed
successfuly.

Dependencies
------------

N/A

Example Playbook
----------------

Suppose one uses a `bedrock.ini` inventory file that contains these hosts:

    [backup_clients]
    bedroom-pc.bedrock.lan
    living-pc.bedrock.lan

and a `borgbackup_clients.yml` playbook that looks like this:

```
- hosts: "backup_clients"
  become: true
  roles:
    - role: borgbackup_clients
```

one could call ansible like:

```
$ ansible-playbook -i bedrock.ini borgbackup_clients.yml
```

License
-------

BSD

Author Information
------------------

[psa90](https://gitlab.com/psa90)

[^1]: https://www.borgbackup.org/
